import os
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator


mpl.use('pgf')
pgf_dc = {
    'text.usetex': True,
    'pgf.rcfonts': False
}
mpl.rcParams.update(pgf_dc)

filename = "sumplot.pdf"

# Load data
df_ATLAS = pd.read_csv(
    "HEPData-ins2104706-v1-Figure_3.csv",
    comment="#",
    index_col=[0])
df_ATLAS["channel"] = df_ATLAS.index
df_ATLAS["mue"] = df_ATLAS["Data"]
df_ATLAS["down"] = -df_ATLAS["Total uncertainty -"]
df_ATLAS["up"] = df_ATLAS["Total uncertainty +"]
df_CMS = pd.read_csv(
    "HEPData-ins2104672-v1-Production_mode_times_decay.csv",
    comment="#")
df_CMS["channel"] = df_CMS["Parameter"]
df_CMS["mue"] = df_CMS["strength modifier"]
df_CMS["down"] = -df_CMS["Stat.+Syst. -"]
df_CMS["up"] = df_CMS["Stat.+Syst. +"]

# Set style
fig, ax = plt.subplots(
    1,
    6,
    figsize=(8.0, 4.0),
    sharey=True,
)
fig.subplots_adjust(wspace=0)
col_ATLAS = "C0"
col_CMS = "C1"
col_comb = "black"
capsize=4
markersize=4
col_smline = "gray"
for x in ax:
    x.tick_params(axis="y", length=0)
plt.title(
    r"$\textrm{\textbf{LHC Run 2:~}}$"
    r"$\sigma \times \mathrm{BR}~$"
    r"$\textrm{normalized to SM prediction}$",
    loc="right")



def main():

    set_yaxis()

    # First column: ggh production
    make_ggh_yy()
    make_ggh_zz()
    make_ggh_ww()
    make_ggh_ll()
    set_ggh_xaxis()

    # Second column: vbf production
    make_vbf_yy()
    make_vbf_zz()
    make_vbf_ww()
    make_vbf_ll()
    make_vbf_bb() # CMS results: 2308.01253
    set_vbf_xaxis()


    # Third column: wh production
    make_wh_yy()
    make_wh_zz() # Only CMS result
    make_wh_ww() # New results: ATLAS-CONF-2022-067 (Fig. 9)
                 #              CMS: 2206.09466 (Fig. 23)
    make_wh_ll() # New ATLAS results shown at Higgs 23 (no reference given...)
                 # https://indico.ihep.ac.cn/event/18025/contributions/
                 #     133676/attachments/73986/90562/Slides_Higgs2023_ADM_v4.pdf
    make_wh_bb() # New CMS results: 2312.07562, vorher: CMS-HIG-20-001 (Fig. 5 right)
                 # New ATLAS result: ATLAS-CONF-2024-010, Fig. 12
    set_wh_xaxis()

    # Fourth column: zh production
    make_zh_yy()
    make_zh_zz() # Not yet measured (?)
    make_zh_ww() # New ATLAS results: ATLAS-CONF-2022-067 (Fig. 9)
    make_zh_ll() # New ATLAS results shown at Higgs 23 (no reference given...)
                 # https://indico.ihep.ac.cn/event/18025/contributions/133676/
                 #     attachments/73986/90562/Slides_Higgs2023_ADM_v4.pdf
    make_zh_bb() # New CMS results: 2312.07562, vorher: CMS-HIG-20-001 (Fig. 5 right)
                 # New ATLAS result: ATLAS-CONF-2024-010, Fig. 12
    set_zh_xaxis()


    # Fifth column: vh production (wh + zh combined)
    make_vh_yy() # ATLAS: no VH value given in 2207.00348
                 # CMS: VH not shown in summary plot
                 # Instead take value from 2103.06956 Fig. 16
    make_vh_zz() # Only ATLAS, CMS no combined ZH + WH value given in 2103.04956
    make_vh_ww() # Only ATLAS results: ATLAS-CONF-2022-067 (Fig. 9)
                 # CMS no combined ZH + WH value given in 2206.09466
    make_vh_ll() # New ATLAS results shown at Higgs 23 (no reference given...)
                 # https://indico.ihep.ac.cn/event/18025/contributions/133676/
                 #     attachments/73986/90562/Slides_Higgs2023_ADM_v4.pdf
                 # CMS: VH not shown in summary plot
                 # instead take numbers from 2204.12957 Sect. 11
    make_vh_bb() # ATLAS VH result: ATLAS-CONF-2024-010
                 # Number taken here from 2007.02873 Sect. 9.1
                 # New CMS results: 2312.07562, vorher: CMS-HIG-20-001
    set_vh_xaxis()


    # Sixth column: ttH+tH
    make_tth_yy()
    make_tth_zz()
    make_tth_ww()
    make_tth_ll()
    make_tth_bb() # CMS new result: CMS-PAS-HIG-19-011 Fig. 8
                  # ATLAS updated the result: 2407.10904
    set_tth_xaxis()

    make_legend()

    save_plot()


def set_yaxis():
    ax[0].set_ylim(-0.5, 4.5)
    ax[0].set_yticks(
        [0.0, 1.0, 2.0, 3.0, 4.0],
        [r"$b \bar b \,$", r"$\tau^+ \tau^-$", r"$WW^*$", r"$ZZ^*$", r"$\gamma\gamma$"])


def get_comb(mue1, err1d, err1u, mue2, err2d, err2u):
    symerr1 = (err1d + err1u) / 2
    symerr2 = (err2d + err2u) / 2
    muecomb = (mue1 / symerr1**2 + mue2 / symerr2**2) / \
        (1. / symerr1**2 + 1. / symerr2**2)
    errcomb = np.sqrt(1. / (1. / symerr1**2 + 1. / symerr2**2))
    return muecomb, errcomb


def make_ggh_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"ggF + $bbH \times \gamma\gamma$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS,
    )
    # Plot again without caps for legend
    ax[0].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=0.0,
        color=col_ATLAS,
#       label=r"$\textrm{ATLAS}~(139~\mathrm{fb}^{-1})$"
        label=r"$\textrm{ATLAS}$"
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ggH}^{\gamma\gamma}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS,
    )
    # Plot again without caps for legend
    ax[0].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=0.0,
        color=col_CMS,
#       label=r"$\textrm{CMS}~(138~\mathrm{fb}^{-1})$"
        label=r"$\textrm{CMS}$"
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[0].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb,
    )
    # Plot again without caps for legend
    ax[0].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=0.0,
        color=col_comb,
#       label=r"$\textrm{Weighted average}$"
        label=r"$\textrm{avg.}$"
    )


def make_ggh_zz():
    ypos = 3.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"ggF + $bbH \times ZZ$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ggH}^{ZZ}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[0].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_ggh_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"ggF + $bbH \times WW$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ggH}^{WW}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[0].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_ggh_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"ggF + $bbH \times \tau\tau$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ggH}^{\tau\tau}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[0].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[0].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def set_ggh_xaxis():
    ax[0].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[0].set_xlabel(r"$\textrm{ggH}$")
    majorLocator = MultipleLocator(0.5)
    minorLocator = MultipleLocator(0.1)
    ax[0].xaxis.set_major_locator(majorLocator)
    ax[0].xaxis.set_minor_locator(minorLocator)
    ax[0].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_vbf_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"VBF $\times \gamma\gamma$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{VBF}^{\gamma\gamma}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[1].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_vbf_zz():
    ypos = 3.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"VBF $\times ZZ$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{VBF}^{ZZ}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[1].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_vbf_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"VBF $\times WW$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{VBF}^{WW}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[1].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_vbf_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"VBF $\times \tau\tau$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{VBF}^{\tau\tau}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[1].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[1].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_vbf_bb():
    ypos = 0.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_CMS = 0.97
    mue_CMS_down = 0.39
    mue_CMS_up = 0.45
    ax[1].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    ax[1].text(
        mue_CMS + 0.7 * mue_CMS_up,
        ypos_CMS - 0.24,
        r"$\textrm{NEW}$",
        fontdict={
            "color": col_CMS,
            "fontsize": "x-small"
        })


def set_vbf_xaxis():
    ax[1].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[1].set_xlabel(r"$\textrm{VBF}$")
    ax[1].set_xlim(left=0.0)
    majorLocator = MultipleLocator(0.5)
    minorLocator = MultipleLocator(0.1)
    ax[1].xaxis.set_major_locator(majorLocator)
    ax[1].xaxis.set_minor_locator(minorLocator)
    ax[1].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_wh_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$WH \times \gamma\gamma$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[2].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{WH}^{\gamma\gamma}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[2].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[2].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_wh_zz():
    ypos = 3.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{WH}^{ZZ}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[2].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )


def make_wh_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 0.45
    mue_ATLAS_down = 0.30
    mue_ATLAS_up = 0.32
    ax[2].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
#   ax[2].text(
#       mue_ATLAS + 1.2 * mue_ATLAS_up,
#       ypos_ATLAS - 0.1,
#       r"$\textrm{NEW}$",
#       fontdict={
#           "color": col_ATLAS,
#           "fontsize": "x-small"
#       })
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{WH}^{WW}$"]
#   mue_CMS = dfi["mue"].item()
#   mue_CMS_down = dfi["down"].item()
#   mue_CMS_up = dfi["up"].item()
    mue_CMS = 2.2
    mue_CMS_down = 0.6
    mue_CMS_up = 0.6
    ax[2].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[2].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_wh_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 1.48
    mue_ATLAS_down = 0.50
    mue_ATLAS_up = 0.56
    ax[2].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
#   ax[2].text(
#       mue_ATLAS + 1.2 * mue_ATLAS_up,
#       ypos_ATLAS - 0.1,
#       r"$\textrm{NEW}$",
#       fontdict={
#           "color": col_ATLAS,
#           "fontsize": "x-small"
#       })
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{WH}^{\tau\tau}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[2].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[2].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_wh_bb():
    ypos = 0.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$WH \times bb$"]
#     mue_ATLAS = dfi["mue"].item()
#     mue_ATLAS_down = dfi["down"].item()
#     mue_ATLAS_up = dfi["up"].item()
    mue_ATLAS = 0.95
    mue_ATLAS_down = 0.19
    mue_ATLAS_up = 0.21
    ax[2].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    mue_CMS = 1.31
    mue_CMS_down = np.sqrt(0.24**2 + 0.26**2)
    mue_CMS_up = mue_CMS_down
    ax[2].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[2].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def set_wh_xaxis():
    ax[2].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[2].set_xlabel(r"$\textrm{WH}$")
    ax[2].set_xlim(left=0.0)
    majorLocator = MultipleLocator(1.0)
    minorLocator = MultipleLocator(0.1)
    ax[2].xaxis.set_major_locator(majorLocator)
    ax[2].xaxis.set_minor_locator(minorLocator)
    ax[2].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_zh_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ZH \times \gamma\gamma$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[3].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ZH}^{\gamma\gamma}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[3].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[3].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_zh_zz():
    pass


def make_zh_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 1.64
    mue_ATLAS_down = 0.46
    mue_ATLAS_up = 0.53
    ax[3].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
#   ax[3].text(
#       mue_ATLAS - 3.0 * mue_ATLAS_up,
#       ypos_ATLAS - 0.1,
#       r"$\textrm{NEW}$",
#       fontdict={
#           "color": col_ATLAS,
#           "fontsize": "x-small"
#       })
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ZH}^{WW}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[3].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[3].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_zh_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 1.09
    mue_ATLAS_down = 0.44
    mue_ATLAS_up = 0.51
    ax[3].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
#   ax[3].text(
#       mue_ATLAS + 1.2 * mue_ATLAS_up,
#       ypos_ATLAS - 0.1,
#       r"$\textrm{NEW}$",
#       fontdict={
#           "color": col_ATLAS,
#           "fontsize": "x-small"
#       })
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ZH}^{\tau\tau}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[3].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[3].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_zh_bb():
    ypos = 0.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ZH \times bb$"]
#   mue_ATLAS = dfi["mue"].item()
#   mue_ATLAS_down = dfi["down"].item()
#   mue_ATLAS_up = dfi["up"].item()
    mue_ATLAS = 0.87
    mue_ATLAS_down = 0.20
    mue_ATLAS_up = 0.23
    ax[3].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    mue_CMS = 1.07
    mue_CMS_down = np.sqrt(0.17**2 + 0.17**2)
    mue_CMS_up = mue_CMS_down
    ax[3].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[3].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def set_zh_xaxis():
    ax[3].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[3].set_xlabel(r"$\textrm{ZH}$")
    ax[3].set_xlim(right=2.7)
    majorLocator = MultipleLocator(1.0)
    minorLocator = MultipleLocator(0.1)
    ax[3].xaxis.set_major_locator(majorLocator)
    ax[3].xaxis.set_minor_locator(minorLocator)
    ax[3].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_vh_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_CMS = 1.34
    mue_CMS_down = 0.33
    mue_CMS_up = 0.36
    ax[4].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )


def make_vh_zz():
    ypos = 3.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$VH \times ZZ$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[4].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )


def make_vh_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 0.92
    mue_ATLAS_down = 0.23
    mue_ATLAS_up = 0.25
    ax[4].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
#   ax[4].text(
#       mue_ATLAS + 1.1 * mue_ATLAS_up,
#       ypos_ATLAS - 0.1,
#       r"$\textrm{NEW}$",
#       fontdict={
#           "color": col_ATLAS,
#           "fontsize": "x-small"
#       })


def make_vh_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    mue_ATLAS = 0.98
    mue_ATLAS_down = 0.58
    mue_ATLAS_up = 0.61
    ax[4].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    mue_CMS = 1.79
    mue_CMS_down = 0.45
    mue_CMS_up = mue_CMS_down
    ax[4].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[4].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_vh_bb():
    ypos = 0.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
#   mue_ATLAS = 1.02
#   mue_ATLAS_down = 0.17
#   mue_ATLAS_up = 0.18
    mue_ATLAS = 0.91
    mue_ATLAS_down = 0.14
    mue_ATLAS_up = 0.16
    ax[4].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    mue_CMS = 1.15
    mue_CMS_down = 0.20
    mue_CMS_up = 0.22
    ax[4].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[4].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def set_vh_xaxis():
    ax[4].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[4].set_xlabel(r"$\textrm{VH}$")
    ax[4].set_xlim(right=2.5)
    majorLocator = MultipleLocator(1.0)
    minorLocator = MultipleLocator(0.1)
    ax[4].xaxis.set_major_locator(majorLocator)
    ax[4].xaxis.set_minor_locator(minorLocator)
    ax[4].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_tth_yy():
    ypos = 4.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ttH \times \gamma\gamma$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ttH+tH}^{\gamma\gamma}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[5].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_tth_zz():
    ypos = 3.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ttH+tH \times ZZ$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ttH+tH}^{ZZ}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_up, mue_CMS_up)
    ax[5].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_tth_ww():
    ypos = 2.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ttH+tH \times WW$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ttH+tH}^{WW}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[5].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_tth_ll():
    ypos = 1.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    dfi = df_ATLAS[df_ATLAS["channel"] == r"$ttH+tH \times \tau\tau$"]
    mue_ATLAS = dfi["mue"].item()
    mue_ATLAS_down = dfi["down"].item()
    mue_ATLAS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    dfi = df_CMS[df_CMS["channel"] == r"$\mu_{ttH+tH}^{\tau\tau}$"]
    mue_CMS = dfi["mue"].item()
    mue_CMS_down = dfi["down"].item()
    mue_CMS_up = dfi["up"].item()
    ax[5].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[5].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def make_tth_bb():
    ypos = 0.0
    ypos_ATLAS = -0.2 + ypos
    ypos_CMS = 0.2 + ypos
    # dfi = df_ATLAS[df_ATLAS["channel"] == r"$ttH+tH \times bb$"]
    # mue_ATLAS = dfi["mue"].item()
    # mue_ATLAS_down = dfi["down"].item()
    # mue_ATLAS_up = dfi["up"].item()
    mue_ATLAS = 0.81
    mue_ATLAS_down = 0.19
    mue_ATLAS_up = 0.22
    ax[5].errorbar(
        mue_ATLAS,
        y=ypos_ATLAS,
        xerr=[[mue_ATLAS_down], [mue_ATLAS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_ATLAS
    )
    ax[5].text(
        mue_ATLAS + 1.2 * mue_ATLAS_up,
        ypos_ATLAS - 0.1,
        r"$\textrm{NEW}$",
        fontdict={
            "color": col_ATLAS,
            "fontsize": "x-small"
        })
    mue_CMS = 0.33
    mue_CMS_down = 0.26
    mue_CMS_up = mue_CMS_down
    ax[5].errorbar(
        mue_CMS,
        y=ypos_CMS,
        xerr=[[mue_CMS_down], [mue_CMS_up]],
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_CMS
    )
    mue_comb, delta_comb = get_comb(
        mue_ATLAS, mue_ATLAS_down, mue_ATLAS_up,
        mue_CMS, mue_CMS_down, mue_CMS_up)
    ax[5].errorbar(
        mue_comb,
        y=ypos,
        xerr=delta_comb,
        marker="o",
        markersize=markersize,
        capsize=capsize,
        color=col_comb
    )


def set_tth_xaxis():
    ax[5].axvline(
        1.0,
        linestyle=":",
        color=col_smline
    )
    ax[5].set_xlabel(r"$\textrm{ttH(+tH)}$")
    ax[5].set_xlim(
        left=0.0,
        right=2.6)
    majorLocator = MultipleLocator(1.0)
    minorLocator = MultipleLocator(0.1)
    ax[5].xaxis.set_major_locator(majorLocator)
    ax[5].xaxis.set_minor_locator(minorLocator)
    ax[5].tick_params(
        axis='x',
        direction='in',
        which='both',
        top=True)


def make_legend():
    fig.legend(
        loc=(0.13, 0.126),
        labelspacing=0.2,
        framealpha=0.4)


def save_plot():
    plt.savefig(filename)
    os.system("pdfcrop " + filename + " " + filename)


if __name__ == '__main__':
    main()
